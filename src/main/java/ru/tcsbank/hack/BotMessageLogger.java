package ru.tcsbank.hack;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by user on 24.04.2016.
 */
@Component
public class BotMessageLogger {
    private static Logger LOGGER = LoggerFactory.getLogger(BotMessageLogger.class);
    private boolean running;
    private ConcurrentLinkedDeque<BotMessageWithResponse> messageQueue = new ConcurrentLinkedDeque<>();
    private Thread queuePolling;
    private static Pattern urlPattern = Pattern.compile("^https?:\\/\\/([a-zA-Z_0-9\\.]+).*$");
    private Map<String, SeekableByteChannel> urlChannels = new ConcurrentHashMap<>();
    static Set<OpenOption> fileOptions = new HashSet<>();
    static {
        fileOptions.add(StandardOpenOption.APPEND);
        fileOptions.add(StandardOpenOption.CREATE);
    }
    private static char CSV_DELIMITER = ';';
    private static char CSV_DELIMITER_REPLACE = '.';

    @PostConstruct
    public void init() {
        running = true;
        queuePolling = new Thread(() -> {
            while (running) {
                BotMessageWithResponse nextMessage = messageQueue.poll();
                if (nextMessage == null) {
                    try {
                        Thread.sleep(1000L);
                    } catch (InterruptedException e) {
                        // ok
                    }
                    continue;
                }
                write(nextMessage);
            }
        });
        queuePolling.start();
    }

    @PreDestroy
    public void shutdown() {
        running = false;
        queuePolling.interrupt();
        urlChannels.values().stream().forEach(channel -> {
            try {
                channel.close();
            } catch (IOException e) {
                LOGGER.warn("failed to close channel", e);
            }
        });
    }

    private void write(BotMessageWithResponse message) {
        try {
            String url = message.getUrl();
            String address = resolveAddress(url).orElse("unknown-address");
            LOGGER.debug("writing message for address " + address);
            String fileName = address + ".csv";
            StringBuffer buffer = new StringBuffer(message.getMessage().length() + message.getResponse().length() + 3);
            buffer
                    .append(message.getMessage()
                            .replace(CSV_DELIMITER, CSV_DELIMITER_REPLACE)
                            .replace('\n', ' ')
                            .replace('\r', ' '))
                    .append(CSV_DELIMITER)
                    .append(message.getResponse()
                            .replace(CSV_DELIMITER, CSV_DELIMITER_REPLACE)
                            .replace('\n', ' ')
                            .replace('\r', ' '))
                    .append("\r\n");
            SeekableByteChannel fileChannel = urlChannels.get(url);
            if (fileChannel == null) {
                fileChannel = Files.newByteChannel(Paths.get(fileName), fileOptions);
                urlChannels.put(url, fileChannel);
            }
            fileChannel.write(
                    ByteBuffer.wrap(
                            buffer.toString().getBytes(Charset.forName("UTF-8"))));
        } catch (Exception e) {
            LOGGER.error("failed to write log", e);
        }
    }

    private static Optional<String> resolveAddress(String url) {
        Matcher matcher = urlPattern.matcher(url);
        if (matcher.matches()) {
            return Optional.ofNullable(matcher.group(1));
        } else {
            LOGGER.warn("cannot resolve url: " + url);
            return Optional.empty();
        }
    }

    public void log(BotMessageWithResponse message) {
        if (running) {
            messageQueue.offer(message);
        } else {
            LOGGER.debug("Logger stopped, ignoring message for " + message.getUrl());
        }
    }
}
