package ru.tcsbank.hack;

/**
 * Created by user on 24.04.2016.
 */
public class BotMessageWithResponse extends BotMessage {
    private String response;

    public BotMessageWithResponse(BotMessage message, String response) {
        super(message.getUrl(), message.getMessage());
        this.response = response;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
