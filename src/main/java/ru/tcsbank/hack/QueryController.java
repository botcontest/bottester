package ru.tcsbank.hack;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.Collections;

/**
 * Created by k.devyatkin on 21.04.2016.
 */
@RestController
public class QueryController {

    @Autowired
    private BotMessageLogger msgLogger;

    @RequestMapping(path = "/doQuery", method = RequestMethod.POST)
    public BotMessage doQuery(@RequestBody BotMessage message){
        try{
        RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters()
                    .add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
            String response = restTemplate.postForObject(message.getUrl(), message.getMessage(), String.class);
            msgLogger.log(new BotMessageWithResponse(message, response));
            message.setMessage(response);
            return message;

        } catch (Throwable e){
            return new BotMessage(message.getUrl(), "Ошибка!!! "+e.getMessage());

        }
    }


    @RequestMapping(path = "/bot1", method = RequestMethod.POST,produces = "text/plain;charset=UTF-8", consumes = "text/plain;charset=UTF-8")
    public String echoBot1(@RequestBody String query){
        return "Бот1 отвечает на вопрос "+ query;
    }


    @RequestMapping(path = "/bot2", method = RequestMethod.POST)
    public String echoBot2(@RequestBody String query){
        return "Бот2 отвечает на вопрос "+ query;
    }

    @RequestMapping(path = "/bot3", method = RequestMethod.POST)
    public String echoBot3(@RequestBody String query){
        return "Бот3 отвечает на вопрос "+ query;
    }

}
