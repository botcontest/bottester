package ru.tcsbank.hack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BotTesterApplication {

	public static void main(String[] args) {
		SpringApplication.run(BotTesterApplication.class, args);
	}
}
