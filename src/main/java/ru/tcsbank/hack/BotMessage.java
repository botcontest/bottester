package ru.tcsbank.hack;

/**
 * Created by k.devyatkin on 21.04.2016.
 */
public class BotMessage {
    private String url;
    private String message;

    public BotMessage() {
    }

    public BotMessage(String url, String message) {
        this.url = url;
        this.message = message;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
