app = angular.module("appModule", ['ngResource']);

app.directive('botDialog', ['$http', function($http){
    return {
        restrict: 'AE',
        templateUrl: 'bot-dialog.html',
        replace: 'true',
        scope: {
        },
        controller: function($scope){
            this.url='';
            $scope.chat = '';

            $scope.$on('doQuery', function(event, data){
               console.log('doQuery');
                $scope.doQuery(data.queryText);
            });

            $scope.doQuery = function (queryText) {
                if ($scope.botUrl && $scope.botUrl.length > 0) {
                    $scope.chat = '';
                    $scope.chat = $scope.chat + 'Q: ' + queryText + '\n';
                    $http.post('doQuery', {url: $scope.botUrl, message: queryText}).then(function (response) {
                        console.log(response.message);
                        $scope.chat = $scope.chat + 'A: ' + response.data.message + '\n';
                    })
                }
            };

            this.clear = function(){
                console.log('clear')
            }
        },
        link: function(scope,element,attrs){
            /*setInterval(function(){
                var ta = element.find('textarea')
                ta.scrollTop(ta[0].scrollHeight - ta.height());
            }, 200);*/


        }

    }
}]);

app.directive('ngEnter', function() {
    return function(scope, element, attrs) {
        element.bind("keydown keypress", function(event) {
            if(event.which === 13) {
                scope.$apply(function(){
                    scope.$eval(attrs.ngEnter, {'event': event});
                });

                event.preventDefault();
            }
        });
    };
});

app.controller("baseCtrl", ['$resource','$scope', function($resource, $scope) {


    $scope.queryText = '';

    $scope.doQuery = function(){
        $scope.$broadcast('doQuery',{queryText: $scope.queryText});
    }
}]);


